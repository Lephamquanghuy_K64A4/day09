<?php
    require __DIR__ . '/question.php';

    session_start();
    if($_SERVER['REQUEST_METHOD']=='POST'){
        $_SESSION['page1'] ="";
        $score=0;
        $_SESSION['answers'] = "";
        foreach($_POST['answers'] as $index => $answer ){
            if( array_key_exists( (int)$index, $questions ) ){
                if( $questions[(int)$index ]['correct'] == $answer) {
                    $score++;
                }
            }
        }
        $cookie_name = "page1";
        $cookie_vaule = $score;
        setcookie($cookie_name,$cookie_vaule,time() + (86400 * 30), "/");
        if (isset($_POST["Next"]))
            header("location: quizform2.php");
    }

?>

<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<title>QUIZ</title>
<link rel="stylesheet" type="text/css" href="quizform.css" />
</head>
 
<body>
    <div id="page-wrap">
        <h1>Trang Một</h1>
        <form action='quizform.php' method="post">
            <ol>
            <?php foreach( $questions as $questionno => $value ){ ?>

            <li>
                <h4><?php echo $value['question'];?></h4>
                <?php 
                    foreach( $value['answers'] as $letter => $answer ){ 
                        $label = 'question-'.$questionno.'-answers-'.$letter;
                ?>

                <div>
                    <input type="radio" name="answers[ <?php echo $questionno; ?> ]" id="<?php echo $label; ?>" value="<?php echo $letter; ?>"/>
                    <label for="<?php echo $label; ?>"><!--<?php echo $letter; ?>)--> <?php echo $answer; ?> </label>
                </div>

                <?php } 
                ?>
            </li>

            <?php } ?>
            </ol>
            <button name='Next' type='submit'>Next</button>
        </form>
    </div>
</body>
 
</html>