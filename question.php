<?php
    $questions = array(
        1=>array(
            'question' => 'Sei Shōnagon sinh năm bao nhiêu?',
            'answers'=> array(
                'a' => '965',
                'b' => '966',
                'c' => '964',
                'd' => '967'
            ),
            'correct' => 'b'
        ),
        2=>array(
            'question' => 'H2 + O2 ra gì',
            'answers'=> array(
                'a' => 'HOHO',
                'b' => 'H2O',
                'c' => 'H2O2',
                'd' => 'CHOOCH'
            ),
            'correct' => 'b'
        ),
        3=>array(
            'question' => '天気 nghĩa là gì?',
            'answers'=> array(
                'a' => 'Lạnh',
                'b' => 'Tuyết',
                'c' => 'Nóng',
                'd' => 'Thời tiết'
            ),
            'correct' => 'd'
        ),
        4=>array(
            'question' => '幸せ nghĩa là gì?',
            'answers'=> array(
                'a' => 'Vui',
                'b' => 'Buồn',
                'c' => 'Đau khổ',
                'd' => 'Hạnh phúc'
            ),
            'correct' => 'd'
        ),
        5=>array(
            'question' => '名前 nghĩa là gì?',
            'answers'=> array(
                'a' => 'Tên',
                'b' => 'Tuổi',
                'c' => 'Ngày sinh',
                'd' => 'Nơi ở'
            ),
            'correct' => 'a'
        )
    );

    $questions2 = array(
        1=>array(
            'question' => '月がきれ là gì?',
            'answers'=> array(
                'a' => 'Trăng đêm nay đẹp thật',
                'b' => 'Sao đêm nay đẹp thật',
                'c' => 'Trời đêm nay đẹp thật',
                'd' => 'Biển đêm nay đẹp thật'
            ),
            'correct' => 'a'
        ),
        2=>array(
            'question' => '月 nghĩa là gì',
            'answers'=> array(
                'a' => 'Trăng',
                'b' => 'Sao',
                'c' => 'Trời',
                'd' => 'Biển'
            ),
            'correct' => 'a'
        ),
        3=>array(
            'question' => '星 nghĩa là gì?',
            'answers'=> array(
                'a' => 'Trăng',
                'b' => 'Sao',
                'c' => 'Trời',
                'd' => 'Biển'
            ),
            'correct' => 'b'
        ),
        4=>array(
            'question' => '海 nghĩa là gì?',
            'answers'=> array(
                'a' => 'Trăng',
                'b' => 'Sao',
                'c' => 'Trời',
                'd' => 'Biển'
            ),
            'correct' => 'd'
        ),
        5=>array(
            'question' => '空 nghĩa là gì?',
            'answers'=> array(
                'a' => 'Trăng',
                'b' => 'Sao',
                'c' => 'Trời',
                'd' => 'Biển'
            ),
            'correct' => 'c'
        ),
    );
?>