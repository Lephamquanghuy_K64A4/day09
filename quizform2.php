<?php
    require __DIR__ . '/question.php';
    session_start();
    if( $_SERVER['REQUEST_METHOD']=='POST'){
        $score1=0;
        foreach($_POST['answers'] as $index => $answer ){
            if( array_key_exists( (int)$index, $questions2 ) ){
                if( $questions2[(int)$index ]['correct'] == $answer) {
                    $score1++;
                }
            }
        }
        $cookie_name = "page2";
        $cookie_vaule = $score1;
        setcookie($cookie_name,$cookie_vaule,time() + (86400 * 30), "/");
        if (isset($_POST["submit"]))
            header("location: result.php");
    }
?>

<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<title>QUIZ</title>
<link rel="stylesheet" type="text/css" href="quizform.css"/>
</head>
 
<body>
    <div id="page-wrap">
        <h1>Trang Hai</h1>
        <form action='quizform2.php' method="post">
            <ol>
            <?php foreach( $questions2 as $questionno => $value ){ ?>

            <li>
                <h4><?php echo $value['question'];?></h4>
                <?php 
                    foreach( $value['answers'] as $letter => $answer ){ 
                        $label = 'question-'.$questionno.'-answers-'.$letter;
                ?>

                <div>
                    <input type="radio" name="answers[ <?php echo $questionno; ?> ]" id="<?php echo $label; ?>" value="<?php echo $letter; ?>" />
                    <label for="<?php echo $label; ?>"><!--<?php echo $letter; ?>)--> <?php echo $answer; ?> </label>
                </div>

                <?php } ?>
            </li>

            <?php } ?>
            </ol>
            <button name='submit' type='submit'>Nộp bài</button>
        </form>
    </div>
</body>
 
</html>